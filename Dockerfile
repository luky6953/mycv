FROM nginx:latest

COPY . /usr/share/nginx/html

EXPOSE 8006

CMD ["nginx", "-g", "daemon off;"]
